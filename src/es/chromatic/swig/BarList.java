package es.chromatic.swig; 
 
 
import java.util.ArrayList;
 
 
 
import android.os.Parcel;
 
import android.os.Parcelable;
 
 
 
public class BarList extends ArrayList<Bar> implements Parcelable{
 
 
 
        private static final long serialVersionUID = 663585476779879096L;
 
 
 
        public BarList(){   
 
        }
 
       
 
        public BarList(Parcel in){
 
                readFromParcel(in);
 
        }
 
       
 
        @SuppressWarnings({ "unchecked", "rawtypes" })
 
        public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
 
                public BarList createFromParcel(Parcel in) {
 
                        return new BarList(in);
 
                }
 
 
 
                public Object[] newArray(int arg0) {
 
                        return null;
 
                }
 
 
 
        };
 
       
 
        private void readFromParcel(Parcel in) {
 
                this.clear();
 
 
 
                //First we have to read the list size
 
                int size = in.readInt();
 
 
 
                //Reading remember that we wrote first the Name and later the Phone Number.
 
                //Order is fundamental
 
               
 
                for (int i = 0; i < size; i++) {
 
                        Bar c = new Bar();
 
                        c.setNombre(in.readString());
                        
                        c.setTipo(in.readString());
                        
                        c.setLocalidad(in.readString());
                        
                        c.setProvincia(in.readString());
                        
                        c.setDireccion(in.readString());                      
                        
                        c.setLatitud(in.readString());
                        
                        c.setLongitud(in.readString());
                        
                        c.setID(in.readString());
                        
                        c.setImagen(in.readString());                        
                        
                        c.setTelefono(in.readString());
                        
                        this.add(c);
 
                }
 
               
 
        }
 
 
 
        public int describeContents() {
 
                return 0;
 
        }
 
 
 
        public void writeToParcel(Parcel dest, int flags) {
 
                int size = this.size();
 
                //We have to write the list size, we need him recreating the list
 
                dest.writeInt(size);
 
                //We decided arbitrarily to write first the Name and later the Phone Number.
 
                for (int i = 0; i < size; i++) {
 
                        Bar c = this.get(i);
 
                        dest.writeString(c.getNombre());
 
                        dest.writeString(c.getTipo());
                        
                        dest.writeString(c.getLocalidad());
                        
                        dest.writeString(c.getProvincia());
                        
                        dest.writeString(c.getDireccion());
                        
                        dest.writeString(c.getLatitud());
                        
                        dest.writeString(c.getLongitud());
                        
                        dest.writeString(c.getID());
                        
                        dest.writeString(c.getImagen());
                        
                        dest.writeString(c.getTelefono());
 
                }
 
        }
 
       
 
 
 
}
 
 