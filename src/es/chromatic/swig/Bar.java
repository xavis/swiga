package es.chromatic.swig;

public class Bar{
	String id;
	String nombre;
	String tipo;
	String imagen;
	String telefono;
	String latitud;
	String longitud;
	String direccion;
	String provincia;
	String localidad;
	
	public Bar(String id, String n, String t, String i, String te, String lat, String lon, String dir, String loc, String p){
		
		this.nombre = n;
		this.telefono = te;
		this.imagen = i;
		this.latitud = lat;
		this.longitud = lon;
		this.tipo = t;
		this.id = id;
		this.direccion = dir;
		this.localidad = loc;
		this.provincia = p;
	}
	
	public Bar(){

	}
	
	public void setNombre(String n){
		nombre = n;
	}
	
	public void setTipo(String n){
		tipo = n;
	}
	
	public void setImagen(String n){
		imagen = n;
	}
	
	public void setLatitud(String n){
		latitud = n;
	}
	
	public void setLongitud(String n){
		longitud = n;
	}
	
	public void setID(String n){
		id = n;
	}

	
	public void setTelefono(String n){
		telefono = n;
	}
	
	public void setDireccion(String n){
		direccion = n;
	}
	
	public void setProvincia(String n){
		provincia = n;
	}
	
	public void setLocalidad(String n){
		localidad = n;
	}
	
	public String getLocalidad(){
		return this.localidad;
	}
	
	public String getProvincia(){
		return this.provincia;
	}
	
	public String getDireccion(){
		return this.direccion;
	}
	
	public String getNombre(){
		return this.nombre;
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getImagen(){
		return this.imagen;
	}
	
	public String getLatitud(){
		return this.latitud;
	}
	
	public String getLongitud(){
		return this.longitud;
	}
	
	public String getTelefono(){
		return this.telefono;
	}
	
	public String getTipo(){
		return this.tipo;
	}
}
