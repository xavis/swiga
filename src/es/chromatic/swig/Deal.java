package es.chromatic.swig;

public class Deal {
	BarList bar;
	String nombre;
	String tipo;
	String hasta;
	String descripcion;
	String hora;
	
	//BAR VARIABLES
	
	String barid;
	String barnombre;
	String bartipo;
	String barimagen;
	String bartelefono;
	String barlatitud;
	String barlongitud;
	String bardireccion;
	String barprovincia;
	String barlocalidad;
	
	public Deal(BarList b, String n, String t, String h, String d, String ho){
		
		this.bar = b;
		this.nombre = n;
		this.tipo = t;
		this.hora = ho;
		this.hasta = h;
		this.descripcion = d;
	}
	
	public Deal(){

	}
	
	public void setBar(BarList b){
		bar = b;
	}
	
	public void setNombre(String n){
		nombre = n;
	}
	
	public void setTipo(String n){
		tipo = n;
	}
	
	public void setHora(String n){
		hora = n;
	}
	
	public void setHasta(String n){
		hasta = n;
	}
	
	public void setDescripcion(String n){
		descripcion = n;
	}
	
	public String getTipo(){
		return this.tipo;
	}


	public String getNombre(){
		return this.nombre;
	}
	
	public String getHasta(){
		return this.hasta;
	}
	
	public String getHora(){
		return this.hora;
	}
	
	public String getDescripcion(){
		return this.descripcion;
	}
	
	public BarList getBar(){
		return this.bar;
	}
	
	// BAR METHODS
	
	public void setBarNombre(String n){
		barnombre = n;
	}
	
	public void setBarTipo(String n){
		bartipo = n;
	}
	
	public void setBarImagen(String n){
		barimagen = n;
	}
	
	public void setBarLatitud(String n){
		barlatitud = n;
	}
	
	public void setBarLongitud(String n){
		barlongitud = n;
	}
	
	public void setBarID(String n){
		barid = n;
	}

	
	public void setBarTelefono(String n){
		bartelefono = n;
	}
	
	public void setBarDireccion(String n){
		bardireccion = n;
	}
	
	public void setBarProvincia(String n){
		barprovincia = n;
	}
	
	public void setBarLocalidad(String n){
		barlocalidad = n;
	}
	
	public String getBarLocalidad(){
		return this.barlocalidad;
	}
	
	public String getBarProvincia(){
		return this.barprovincia;
	}
	
	public String getBarDireccion(){
		return this.bardireccion;
	}
	
	public String getBarNombre(){
		return this.barnombre;
	}
	
	public String getBarID(){
		return this.barid;
	}
	
	public String getBarImagen(){
		return this.barimagen;
	}
	
	public String getBarLatitud(){
		return this.barlatitud;
	}
	
	public String getBarLongitud(){
		return this.barlongitud;
	}
	
	public String getBarTelefono(){
		return this.bartelefono;
	}
	
	public String getBarTipo(){
		return this.bartipo;
	}
	
	
	
	
	
	
	
}
