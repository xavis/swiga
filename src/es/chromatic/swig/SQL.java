package es.chromatic.swig;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;


public class SQL {

   private static final String DATABASE_NAME = "favoritos.db";
   private static final int DATABASE_VERSION = 2;
   private static final String DELETEWHERE = "idbar = ?";
   private static final String TABLE_NAME = "bares";
   private static final String[] COLUMNAS = {"id","idbar","nombre","tipo","direccion","localidad","provincia","longitud","latitud","telefono","imagen"};
   
   private Context context;
   private SQLiteDatabase db;

   private SQLiteStatement insertStatement;
   private static final String INSERT = "insert into " + TABLE_NAME + 
		     "("+COLUMNAS[1]+","+COLUMNAS[2]+","+COLUMNAS[3]+","+COLUMNAS[4]+","+COLUMNAS[5]+","+COLUMNAS[6]+","+COLUMNAS[7]+","+COLUMNAS[8]+","+COLUMNAS[9]+","+COLUMNAS[10]+") values (?,?,?,?,?,?,?,?,?,?)";
   private static final String CREATE_DB = "CREATE TABLE " + TABLE_NAME + 
	 "("+COLUMNAS[0]+" INTEGER PRIMARY KEY, "+COLUMNAS[1]+" TEXT, "+COLUMNAS[2]+" TEXT, "+COLUMNAS[3]+" TEXT, "+COLUMNAS[4]+" TEXT, "+COLUMNAS[5]+" TEXT, "+COLUMNAS[6]+" TEXT, "+COLUMNAS[7]+" TEXT, "+COLUMNAS[8]+" TEXT, "+COLUMNAS[9]+" TEXT, "+COLUMNAS[10]+" TEXT)";
   

   
   public SQL(Context context) {
      this.context = context;
      MiOpenHelper openHelper = new MiOpenHelper(this.context);
      this.db = openHelper.getWritableDatabase();
      this.insertStatement = this.db.compileStatement(INSERT);

   }

   public long insert(String id,String nombre, String tipo,String direccion, String localidad, String provincia, String longitud, String latitud, String telefono, String imagen) {
	   
	   SQLiteStatement insertStatement;
	   insertStatement = db.compileStatement(INSERT);
	   insertStatement.bindString(1, id);
	   insertStatement.bindString(2, nombre);
	   insertStatement.bindString(3, tipo);
	   insertStatement.bindString(4, direccion);
	   insertStatement.bindString(5, localidad);
	   insertStatement.bindString(6, provincia);
	   insertStatement.bindString(7, longitud);
	   insertStatement.bindString(8, latitud);
	   insertStatement.bindString(9, telefono);
	   insertStatement.bindString(10, imagen);
	   
	   return insertStatement.executeInsert();

   }

   public int deleteAll() {
	   return db.delete(TABLE_NAME, null, null);
   }
   
   public int delete(String id) {
	   String[] a = new String[1];
	   a[0] = id;
	   return db.delete(TABLE_NAME, DELETEWHERE, a);
   }

   public BarList selectAll() {
	      BarList list = new BarList();
	      Cursor cursor = db.query(TABLE_NAME, COLUMNAS, 
	        null, null, null, null, null);
	      if (cursor.moveToFirst()) {
	         do {
	        	 Bar bar = new Bar();
	        	 bar.setID(cursor.getString(1));
	        	 Log.e("SELECTALL","ID: "+cursor.getString(1)+" - "+bar.id);
	        	 bar.setNombre(cursor.getString(2));
	        	 Log.e("SELECTALL","NOMBRE: "+cursor.getString(2)+" - "+bar.nombre);
	        	 bar.setTipo(cursor.getString(3));
	        	 Log.e("SELECTALL","TIPO: "+cursor.getString(3)+" - "+bar.tipo);
	        	 bar.setDireccion(cursor.getString(4));
	        	 Log.e("SELECTALL","DIRECCION: "+cursor.getString(4)+" - "+bar.direccion);
	        	 bar.setLocalidad(cursor.getString(5));
	        	 bar.setProvincia(cursor.getString(6));
	        	 bar.setLatitud(cursor.getString(7));
	        	 bar.setLongitud(cursor.getString(8));
	        	 bar.setTelefono(cursor.getString(9));
	        	 bar.setImagen(cursor.getString(10));
	        	 list.add(bar); 
	         } while (cursor.moveToNext());
	      }
	      if (cursor != null && !cursor.isClosed()) {
	         cursor.close();
	      }
	      return list;
	   }

   private static class MiOpenHelper extends SQLiteOpenHelper {

      MiOpenHelper(Context context) {
         super(context, DATABASE_NAME, null, DATABASE_VERSION);
      }

      @Override
      public void onCreate(SQLiteDatabase db) {
    	  db.execSQL(CREATE_DB);
      }

      @Override
      public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         Log.w("SQL", "onUpgrade: eliminando tabla si ésta existe, y creándola de nuevo");
         Log.w("SQL","onUpgrade: eliminando tabla si existe, y cre�ndola de nuevo");
         db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
         onCreate(db);
      }
   }
}
