package es.chromatic.swig;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DealDetail extends Activity {
	
	TextView titulo;
	TextView subtitulo;
	TextView hora;
	TextView hasta;
	TextView descripcion;
	Button abar;
	DealList deallist;
	Deal deal;

	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_deal);
	        
	        titulo = (TextView) findViewById(R.id.dealtitulo);
	        subtitulo =  (TextView) findViewById(R.id.dealsubtitulo);
	        hora =  (TextView) findViewById(R.id.Hora);
	        hasta =  (TextView) findViewById(R.id.Hasta);
	        descripcion =  (TextView) findViewById(R.id.Description);
	        abar =  (Button) findViewById(R.id.abar);
	        
	        
	        Bundle b = getIntent().getExtras();
	        
	        deallist = b.getParcelable("Deal"); 
		    deal = deallist.get(0);
		    titulo.setText(deal.nombre);
		    subtitulo.setText(deal.tipo);
		    descripcion.setText(deal.descripcion);
		    hora.setText("- Horario: "+deal.hora);
		    hasta.setText("- Hasta: "+deal.hasta);
		    
		    deal.bar = new BarList();
		    Bar bar = new Bar(deal.barid,deal.barnombre,deal.bartipo,deal.barimagen,deal.bartelefono,deal.barlatitud,deal.barlongitud,deal.bardireccion,deal.barlocalidad,deal.barprovincia);
		    deal.bar.add(bar);
	 }
	
	 
	 public void abar(View v){
		 
		 
		 Intent mainIntent = new Intent(DealDetail.this, BarDetail.class);
			Bundle b = new Bundle();
			BarList barlist1 = new BarList();
			barlist1.add(deal.bar.get(0));
			b.putParcelable("Bar",barlist1);
			mainIntent.putExtras(b);
			DealDetail.this.startActivity(mainIntent);
		 
		 
	 }
	 
	 
	 public void backkk(View view){
			
			finish();
			
		}
		
	public void home(View view){
			
		Intent mainIntent = new Intent(DealDetail.this, Main.class);
			DealDetail.this.startActivity(mainIntent);
			this.finish();
			
		}
	 

}
