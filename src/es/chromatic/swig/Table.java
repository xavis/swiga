package es.chromatic.swig;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.view.ViewGroup.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

@SuppressLint("ParserError")
public class Table extends Activity{

	ArrayList<Deal> array2;
	ArrayList<Bar> array;
	int opcion;
	TextView titulo;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        
        Bundle extras = getIntent().getExtras();
        opcion = extras.getInt("Opcion");
        titulo = (TextView) findViewById(R.id.titulotabla);
        
        
        if(opcion==0){
        
        array = extras.getParcelable("Results");
        
        for(int i=0;i<array.size();i++){
            // get a reference for the TableLayout
            TableLayout table = (TableLayout)findViewById(R.id.TableLayout01);
            // create a new TableRow
            LayoutInflater inflater = getLayoutInflater();
            TableRow row = (TableRow)inflater.inflate(R.layout.customrow, table, false);
            TextView content = (TextView)row.findViewById(R.id.titlerow);
            TextView subcontent = (TextView)row.findViewById(R.id.subtitlerow);
            content.setText(array.get(i).nombre);
            subcontent.setText(array.get(i).tipo);
            // add the TextView  to the new TableRow
            // add the TableRow to the TableLayout
            row.setTag(i);
            row.setOnClickListener(new OnClickListener() {
                
                public void onClick(View v) {
                	 Intent mainIntent = new Intent(Table.this, BarDetail.class);
         	        Bundle b = new Bundle();
         	        BarList barlist1 = new BarList();
         	        int num = (Integer) v.getTag();
         	        Bar barr = array.get(num);
         	        barlist1.add(barr);
         	        b.putParcelable("Bar",barlist1);
         	        mainIntent.putExtras(b);
         			Table.this.startActivity(mainIntent);
    			
                    

                }   });
            
            
            table.addView(row);
        }
        
        
        
        }
        
        else if(opcion==1){
        	titulo.setText("Ofertas Encontradas");
            array2 = extras.getParcelable("Results");
            
            for(int i=0;i<array2.size();i++){
                // get a reference for the TableLayout
                TableLayout table = (TableLayout)findViewById(R.id.TableLayout01);
                // create a new TableRow
                LayoutInflater inflater = getLayoutInflater();
                TableRow row = (TableRow)inflater.inflate(R.layout.customrow, table, false);
                TextView content = (TextView)row.findViewById(R.id.titlerow);
                TextView subcontent = (TextView)row.findViewById(R.id.subtitlerow);
                content.setText(array2.get(i).nombre);
                subcontent.setText(array2.get(i).tipo);
                // add the TextView  to the new TableRow
                // add the TableRow to the TableLayout
                row.setTag(i);
             
                row.setOnClickListener(new OnClickListener() {
                    
                    public void onClick(View v) {
                    	 Intent mainIntent = new Intent(Table.this, DealDetail.class);
             	        Bundle b = new Bundle();
             	        DealList deallist1 = new DealList();
             	        int num = (Integer) v.getTag();
             	        Deal barr = array2.get(num);
             	        deallist1.add(barr);
             	        b.putParcelable("Deal",deallist1);
             	        mainIntent.putExtras(b);
             			Table.this.startActivity(mainIntent);
        			
                        

                    }   });
                
                
                table.addView(row);
            }
            
            
        }
        
        else if(opcion==3){
        	titulo.setText("Favoritos");
        	array = extras.getParcelable("Results");
            
            for(int i=0;i<array.size();i++){
                // get a reference for the TableLayout
                TableLayout table = (TableLayout)findViewById(R.id.TableLayout01);
                // create a new TableRow
                LayoutInflater inflater = getLayoutInflater();
                TableRow row = (TableRow)inflater.inflate(R.layout.customrow, table, false);
                TextView content = (TextView)row.findViewById(R.id.titlerow);
                TextView subcontent = (TextView)row.findViewById(R.id.subtitlerow);
                content.setText(array.get(i).nombre);
                subcontent.setText(array.get(i).tipo);
                // add the TextView  to the new TableRow
                // add the TableRow to the TableLayout
                row.setTag(i);
                row.setOnClickListener(new OnClickListener() {
                    
                    public void onClick(View v) {
                    	 Intent mainIntent = new Intent(Table.this, BarDetail.class);
             	        Bundle b = new Bundle();
             	        BarList barlist1 = new BarList();
             	        int num = (Integer) v.getTag();
             	        Bar barr = array.get(num);
             	        barlist1.add(barr);
             	        b.putParcelable("Bar",barlist1);
             	        mainIntent.putExtras(b);
             			Table.this.startActivity(mainIntent);
        			
                        

                    }   });
                
                
                table.addView(row);
            }
            
        	
        	
        }
        
	}

	
        @Override
        public void onResume(){
        	
        	super.onResume();
        	if(opcion==3){
        	SQL dataHelper = new SQL(getApplicationContext());
        	
        	array = dataHelper.selectAll();
        	
        	 TableLayout table = (TableLayout)findViewById(R.id.TableLayout01);
             table.removeAllViews();
             TextView text = new TextView(this);
             text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
             text.setText("No se han encontrado favoritos");
             text.setGravity(Gravity.CENTER);
             text.setTextSize(16);
             if(array.size()==0) table.addView(text);
             else  if(array.size()>0){
            
            for(int i=0;i<array.size();i++){
                // get a reference for the TableLayout
               
                // create a new TableRow
                LayoutInflater inflater = getLayoutInflater();
                TableRow row = (TableRow)inflater.inflate(R.layout.customrow, table, false);
                TextView content = (TextView)row.findViewById(R.id.titlerow);
                TextView subcontent = (TextView)row.findViewById(R.id.subtitlerow);
                content.setText(array.get(i).nombre);
                subcontent.setText(array.get(i).tipo);
                // add the TextView  to the new TableRow
                // add the TableRow to the TableLayout
                row.setTag(i);
                row.setOnClickListener(new OnClickListener() {
                    
                    public void onClick(View v) {
                    	 Intent mainIntent = new Intent(Table.this, BarDetail.class);
             	        Bundle b = new Bundle();
             	        BarList barlist1 = new BarList();
             	        int num = (Integer) v.getTag();
             	        Bar barr = array.get(num);
             	        barlist1.add(barr);
             	        b.putParcelable("Bar",barlist1);
             	        mainIntent.putExtras(b);
             			Table.this.startActivity(mainIntent);
        			
                        

                    }   });
                
                
                table.addView(row);
            
            }
        	
        }
        	
        	
        }}
        
        @Override
        public void onBackPressed() {
        	Intent mainIntent = new Intent(Table.this, Main.class);
 			Table.this.startActivity(mainIntent);
 			this.finish();
        }
    	
    	
}
	