package es.chromatic.swig;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;

public class Main extends Activity {
	
	
	Button onBar;
	Button onDeal;
	EditText campo;
	Button fav;
	int opcion=0;
	ImageView imagen;
	Button bar1;
	Button bar2;
	Button bar3;
	Button barder;
	Button barizq;
	BarList array;
	Bar currentBar;
	TextView loading;
	int maxcount =0;
	boolean canclick = false;
	ConnectivityManager network;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        onBar = (Button)findViewById(R.id.onBar);
        onDeal = (Button)findViewById(R.id.onDeal);
        fav = (Button)findViewById(R.id.fav);
        campo = (EditText)findViewById(R.id.editText1);
        imagen = (ImageView)findViewById(R.id.imageView1);
        bar1 = (Button)findViewById(R.id.bar1);
        bar2 = (Button)findViewById(R.id.bar2);
        bar3 = (Button)findViewById(R.id.bar3);
        barder = (Button)findViewById(R.id.barder);
        barizq = (Button)findViewById(R.id.barizq);
        bar1.setClickable(false);
        bar2.setClickable(false);
        bar2.setText("");
        bar3.setClickable(false);
        barder.setClickable(false);
        barizq.setClickable(false);
        loading = (TextView) findViewById(R.id.loadtext);
        
        
        
        campo.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	if(keyCode == 66) {
            		if(isNetworkAvailable(0)){
            		Intent mainIntent = new Intent(Main.this, Load.class);
            		mainIntent.putExtra("String", campo.getText().toString());
            		mainIntent.putExtra("Opcion", opcion);
    				Main.this.startActivity(mainIntent);
    				
    				
            		//Toast.makeText(getApplicationContext(), campo.getText(), Toast.LENGTH_SHORT).show();
            		InputMethodManager imm = (InputMethodManager)getSystemService(
            			      Context.INPUT_METHOD_SERVICE);
            			imm.hideSoftInputFromWindow(campo.getWindowToken(), 0);
                    return true;
            		}
                }
                return false;
            }

        });
        
        bar1.setBackgroundDrawable(null);
        bar2.setBackgroundDrawable(null);
        bar3.setBackgroundDrawable(null);
        DownloadFilesTask1 download = new DownloadFilesTask1();
        download.execute();
        
    }
    
    public boolean isNetworkAvailable(int o) {
    	 Context context = getApplicationContext();
    	 ConnectivityManager connectivity = (ConnectivityManager) 
    	    context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	 if (connectivity == null&&o==0) {
    			
	 Toast.makeText(getApplicationContext(), "No hay conexi�n", Toast.LENGTH_SHORT).show();
			
		 
    	 } else {
       	  NetworkInfo[] info = connectivity.getAllNetworkInfo();
       	  if (info != null) {
       	   for (int i = 0; i < info.length; i++) {
       	    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
       	     return true;
    	    }
    	   }
    	  }
    	 }
    	 if(o==0)Toast.makeText(getApplicationContext(), "No hay conexi�n", Toast.LENGTH_SHORT).show();
    	 return false;
    	}
    
 public void changeBar(View v){
    	
    	if(opcion==1){
    		imagen.setImageResource(R.drawable.busca_bares);
    		
    		opcion=0;
    		
    	}
    	    	
    }
    
    
    public void changeDeal(View v){
    	
    	if(opcion==0){
    		imagen.setImageResource(R.drawable.busca_ofertas);
    		
    		opcion=1;
    		
    	}
    	
    }
    
    public void onFav(View v){
    	
    	SQL dataHelper = new SQL(getApplicationContext());
    	BarList array = dataHelper.selectAll();
        Intent mainIntent = new Intent(Main.this, Table.class);
        Bundle b = new Bundle();
        b.putParcelable("Results", array);
        b.putInt("Opcion", 3);
        mainIntent.putExtras(b);
		Main.this.startActivity(mainIntent);
		
    	
    }

    
    
public void onRightClick(View v){
	if(canclick==true){
	bar1.setBackgroundDrawable(null);
    bar2.setBackgroundDrawable(null);
    bar3.setBackgroundDrawable(null);
    loading.setText("Cargando...");
	if(array.size()>0&&array.size()>1){
	canclick=false;
	Log.e("OnRight","ENTRA");
	
	Bar last = array.get(array.size()-1);
	BarList sustituto = new BarList();
	sustituto.add(last);
	for(int i=0;i<array.size()-1;i++){
		
		sustituto.add(array.get(i));
		
	}
	
	array=sustituto;
	this.changeBars();}
	}
	
}


public void onLeftClick(View v){
	
	if(canclick==true){
	 bar1.setBackgroundDrawable(null);
     bar2.setBackgroundDrawable(null);
     bar3.setBackgroundDrawable(null);
     loading.setText("Cargando...");
	if(array.size()>0&&array.size()>1){
	canclick=false;
	Log.e("OnLeft","ENTRA");

	Bar last = array.get(0);
	BarList sustituto = new BarList();
	for(int i=1;i<array.size();i++){
		
		sustituto.add(array.get(i));
		
	}
	sustituto.add(last);
	array=sustituto;
	this.changeBars();}}
	
	
}
    
    
    private class DownloadFilesTask1 extends AsyncTask<Void, Integer, Void> {
	    
		 
		 protected Void doInBackground(Void... voids) {
			 if(isNetworkAvailable(1)){
			 try {
				
				
				 URL text = new URL("http://www.swagger.hostoi.com/bar_search.php?key=");
				 
				 array = new BarList();
				 
				 XmlPullParserFactory parserCreator =
							 
							 XmlPullParserFactory.newInstance();
					 
					 XmlPullParser parser = parserCreator.newPullParser();
					 
					 
					 parser.setInput(text.openStream(), null);
					 
					 int parserEvent = parser.getEventType();
					 
					 while (parserEvent != XmlPullParser.END_DOCUMENT) {
						 
						 switch (parserEvent) {
						 
						 case XmlPullParser.START_DOCUMENT:						 
							 break;
							 
						 case XmlPullParser.END_DOCUMENT:
							 
							 this.onPostExecute();
							 
							 break;
							 
						 case XmlPullParser.START_TAG:
							 
							 String tag = parser.getName();
							 
							 if (tag.equalsIgnoreCase("bar")) {
								 
								 Log.i("XML","Creamos bar");
								 
								 currentBar = new Bar();							 
										 
								 
							 }
							 
							 else if(tag.equalsIgnoreCase("id")){
								 
								 currentBar.setID(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("nombre")){
								 
								 currentBar.setNombre(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("tipo")){
								 
								 currentBar.setTipo(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("localidad")){
								 
								 currentBar.setLocalidad(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("provincia")){
								 
								 currentBar.setProvincia(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("CoordX")){
								 
								 currentBar.setLongitud(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("CoordY")){
								 
								 currentBar.setLatitud(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("direccion")){
								 
								 currentBar.setDireccion(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("telefono")){
								 
								 currentBar.setTelefono(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("imagen")){
								 
								 currentBar.setImagen(parser.nextText());								 
							 
							 }
							 
							 
							 
							 break;
							 
						 case XmlPullParser.END_TAG:	
							 
							 	String tag2 = parser.getName();
							 
							 if (tag2.equalsIgnoreCase("bar")) {
								 
								 Log.i("XML","a�adimos bar");
								 
								 array.add(currentBar);
								 
								 currentBar = null;
										 
								 
							 }
							 
							 else if (tag2.equalsIgnoreCase("result")) {
								 
								 this.onPostExecute();
										 
								 
							 }
							 break;
							 
						 } 
						 
						 parserEvent = parser.next();
						 
					 }
					 
			 } catch (Exception e) {				 
				 Log.e("Net", "Error in network call", e);
				 
			 }
		 }
		 
			 else{ runOnUiThread(new Runnable() {
			     public void run() {loading.setText("No hay conexi�n");}}); }
		        return null;
		     }

		     protected void onProgressUpdate(Integer... progress) {
		        
		     }

		     protected void onPostExecute() {
		    	 Log.i("Fin","ENTRA AL FINAL");
	    	 
	    	 if(array.size()==1){
	    		 new Thread(new Runnable() {		    
	 	 		    public void run() {
	 	 		    	
	 	 		    	try{
	 	 		    		
	  			    		InputStream is= new	URL(array.get(0).imagen).openStream();
	  			    		@SuppressWarnings("deprecation")
	 						final
	  						Drawable imagen = new BitmapDrawable(BitmapFactory.decodeStream(is));
	  			    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
	  			    		bar1.post(new Runnable() {
	  			    			public void run() {
	  			    			bar1.setBackgroundDrawable(imagen);
	  			    			loading.setText("");
	  			    			bar1.setOnClickListener(new OnClickListener() {
	  			                    
	  			                    public void onClick(View v) {
	  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
	  			             	        Bundle b = new Bundle();
	  			             	        BarList barlist1 = new BarList();
	  			             	        Bar barr = array.get(0);
	  			             	        barlist1.add(barr);
	  			             	        b.putParcelable("Bar",barlist1);
	  			             	        mainIntent.putExtras(b);
	  			             			Main.this.startActivity(mainIntent);
	  			        			
	  			                        

	  			                    }   });
	  			    			}
	  			    		});
	  			    		bar2.post(new Runnable() {
	  			    			public void run() {
	  			    			bar2.setBackgroundDrawable(imagen);
	  			    			bar2.setOnClickListener(new OnClickListener() {
	  			                    
	  			                    public void onClick(View v) {
	  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
	  			             	        Bundle b = new Bundle();
	  			             	        BarList barlist1 = new BarList();
	  			             	        Bar barr = array.get(0);
	  			             	        barlist1.add(barr);
	  			             	        b.putParcelable("Bar",barlist1);
	  			             	        mainIntent.putExtras(b);
	  			             			Main.this.startActivity(mainIntent);
	  			        			
	  			                        

	  			                    }   });
	  			    			}
	  			    		});
	  			    		bar3.post(new Runnable() {
	  			    			public void run() {
	  			    			bar3.setBackgroundDrawable(imagen);
	  			    			bar3.setOnClickListener(new OnClickListener() {
	  			                    
	  			                    public void onClick(View v) {
	  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
	  			             	        Bundle b = new Bundle();
	  			             	        BarList barlist1 = new BarList();
	  			             	        Bar barr = array.get(0);
	  			             	        barlist1.add(barr);
	  			             	        b.putParcelable("Bar",barlist1);
	  			             	        mainIntent.putExtras(b);
	  			             			Main.this.startActivity(mainIntent);
	  			        			
	  			                        

	  			                    }   });
	  			    			}
	  			    		});}
	 	 		    	catch(MalformedURLException e1){
	 			    		}catch(IOException e2){
	 			    		}
	 	 		    }
	 	 		    }).start(); 
	    		 
	    		 
	    	 }
	    	 
else if(array.size()==2){
	    		 
	    		 new Thread(new Runnable() {		    
		 	 		    public void run() {
		 	 		    	
		 	 		    	try{
	 	 		    	
	  			    		InputStream is= new	URL(array.get(0).imagen).openStream();
	  			    		@SuppressWarnings("deprecation")
	 						final
	  						Drawable imagen = new BitmapDrawable(BitmapFactory.decodeStream(is));
	  			    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
	  			    		
	  			    		is= new	URL(array.get(1).imagen).openStream();
	  			    		@SuppressWarnings("deprecation")
	 						final
	  						Drawable imagen2 = new BitmapDrawable(BitmapFactory.decodeStream(is));
	  			    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
	  			    		bar1.post(new Runnable() {
	  			    			public void run() {
	  			    			bar1.setBackgroundDrawable(imagen);
	  			    			loading.setText("");
	  			    			bar1.setOnClickListener(new OnClickListener() {
	  			                    
	  			                    public void onClick(View v) {
	  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
	  			             	        Bundle b = new Bundle();
	  			             	        BarList barlist1 = new BarList();
	  			             	        Bar barr = array.get(0);
	  			             	        barlist1.add(barr);
	  			             	        b.putParcelable("Bar",barlist1);
	  			             	        mainIntent.putExtras(b);
	  			             			Main.this.startActivity(mainIntent);
	  			        			
	  			                        

	  			                    }   });
	  			    			}
	  			    		});
	  			    		bar2.post(new Runnable() {
	  			    			public void run() {
	  			    			bar2.setBackgroundDrawable(imagen2);
	  			    			bar2.setOnClickListener(new OnClickListener() {
	  			                    
	  			                    public void onClick(View v) {
	  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
	  			             	        Bundle b = new Bundle();
	  			             	        BarList barlist1 = new BarList();
	  			             	        Bar barr = array.get(1);
	  			             	        barlist1.add(barr);
	  			             	        b.putParcelable("Bar",barlist1);
	  			             	        mainIntent.putExtras(b);
	  			             			Main.this.startActivity(mainIntent);
	  			        			
	  			                        

	  			                    }   });
	  			    			}
	  			    		});
	  			    		bar3.post(new Runnable() {
	  			    			public void run() {
	  			    			bar3.setBackgroundDrawable(imagen);
	  			    			bar3.setOnClickListener(new OnClickListener() {
	  			                    
	  			                    public void onClick(View v) {
	  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
	  			             	        Bundle b = new Bundle();
	  			             	        BarList barlist1 = new BarList();
	  			             	        Bar barr = array.get(0);
	  			             	        barlist1.add(barr);
	  			             	        b.putParcelable("Bar",barlist1);
	  			             	        mainIntent.putExtras(b);
	  			             			Main.this.startActivity(mainIntent);
	  			        			
	  			                        

	  			                    }   });
	  			    			}
	  			    		});}
	 	 		    	catch(MalformedURLException e1){
	 	 		    	}catch(IOException e2){
			    		}
		 	 		    }
	 		    }).start(); 
	    		 
	    	 
	    		 }
	    	 
	    	 else if(array.size()>2){
	    		 new Thread(new Runnable() {		    
		 	 		    public void run() {
		 	 		    	
		 	 		    	try{
	    		 InputStream is= new	URL(array.get(0).imagen).openStream();
		    		@SuppressWarnings("deprecation")
					final
					Drawable imagen = new BitmapDrawable(BitmapFactory.decodeStream(is));
		    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
		    		
		    		is= new	URL(array.get(1).imagen).openStream();
		    		@SuppressWarnings("deprecation")
					final
					Drawable imagen2 = new BitmapDrawable(BitmapFactory.decodeStream(is));
		    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
		    		
		    		is= new	URL(array.get(2).imagen).openStream();
		    		@SuppressWarnings("deprecation")
					final
					Drawable imagen3 = new BitmapDrawable(BitmapFactory.decodeStream(is));
		    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
		    		bar1.post(new Runnable() {
		    			public void run() {
		    			bar1.setBackgroundDrawable(imagen);
		    			loading.setText("");
		    			bar1.setOnClickListener(new OnClickListener() {
			                    
			                    public void onClick(View v) {
			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
			             	        Bundle b = new Bundle();
			             	        BarList barlist1 = new BarList();
			             	        Bar barr = array.get(0);
			             	        barlist1.add(barr);
			             	        b.putParcelable("Bar",barlist1);
			             	        mainIntent.putExtras(b);
			             			Main.this.startActivity(mainIntent);
			        			
			                        

			                    }   });
		    			}
		    		});
		    		bar2.post(new Runnable() {
		    			public void run() {
		    			bar2.setBackgroundDrawable(imagen2);
		    			bar2.setOnClickListener(new OnClickListener() {
			                    
			                    public void onClick(View v) {
			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
			             	        Bundle b = new Bundle();
			             	        BarList barlist1 = new BarList();
			             	        Bar barr = array.get(1);
			             	        barlist1.add(barr);
			             	        b.putParcelable("Bar",barlist1);
			             	        mainIntent.putExtras(b);
			             			Main.this.startActivity(mainIntent);
			        			
			                        

			                    }   });
		    			}
		    		});
		    		bar3.post(new Runnable() {
		    			public void run() {
		    			bar3.setBackgroundDrawable(imagen3);
		    			bar3.setOnClickListener(new OnClickListener() {
			                    
			                    public void onClick(View v) {
			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
			             	        Bundle b = new Bundle();
			             	        BarList barlist1 = new BarList();
			             	        Bar barr = array.get(2);
			             	        barlist1.add(barr);
			             	        b.putParcelable("Bar",barlist1);
			             	        mainIntent.putExtras(b);
			             			Main.this.startActivity(mainIntent);
			        			
			                        

			                    }   });
		    			}
		    		});}
		    	catch(MalformedURLException e1){
		    		}catch(IOException e2){
		    		}
		    }
		    }).start(); 
	    		 
	    		 
	    		 
	    	 }
	    	 
	    	 
		       
	    	 bar1.setClickable(true);
	    	 bar2.setClickable(true);
	    	 
	    	 
	    	 
	    	 bar3.setClickable(true);
	    	 barder.setClickable(true);
	    	 barizq.setClickable(true);
	    	 canclick=true;
	    	 
	    	 Log.e("adad","adasdasdassdsadsadgsajfbadvjeilsehviehlvikesnvlisehnvilesbviesbvlwes");
	    	 }
	 }

    
    public void changeBars(){
   	 if(array.size()==1){
   		 new Thread(new Runnable() {		    
	 		    public void run() {
 	 		    	
 	 		    	try{
 	 		    		
  			    		InputStream is= new	URL(array.get(0).imagen).openStream();
  			    		@SuppressWarnings("deprecation")
 						final
  						Drawable imagen = new BitmapDrawable(BitmapFactory.decodeStream(is));
  			    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
  			    		bar1.post(new Runnable() {
  			    			public void run() {
  			    			bar1.setBackgroundDrawable(imagen);
  			    			loading.setText("");
  			    			bar1.setOnClickListener(new OnClickListener() {
  			                    
  			                    public void onClick(View v) {
  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
  			             	        Bundle b = new Bundle();
  			             	        BarList barlist1 = new BarList();
  			             	        Bar barr = array.get(0);
  			             	        barlist1.add(barr);
  			             	        b.putParcelable("Bar",barlist1);
  			             	        mainIntent.putExtras(b);
  			             			Main.this.startActivity(mainIntent);
  			        			
  			                        

  			                    }   });
  			    			}
  			    		});
  			    		bar2.post(new Runnable() {
  			    			public void run() {
  			    			bar2.setBackgroundDrawable(imagen);
  			    			bar2.setOnClickListener(new OnClickListener() {
  			                    
  			                    public void onClick(View v) {
  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
  			             	        Bundle b = new Bundle();
  			             	        BarList barlist1 = new BarList();
  			             	        Bar barr = array.get(0);
  			             	        barlist1.add(barr);
  			             	        b.putParcelable("Bar",barlist1);
  			             	        mainIntent.putExtras(b);
  			             			Main.this.startActivity(mainIntent);
  			        			
  			                        

  			                    }   });
  			    			}
  			    		});
  			    		bar3.post(new Runnable() {
  			    			public void run() {
  			    			bar3.setBackgroundDrawable(imagen);
  			    			bar3.setOnClickListener(new OnClickListener() {
  			                    
  			                    public void onClick(View v) {
  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
  			             	        Bundle b = new Bundle();
  			             	        BarList barlist1 = new BarList();
  			             	        Bar barr = array.get(0);
  			             	        barlist1.add(barr);
  			             	        b.putParcelable("Bar",barlist1);
  			             	        mainIntent.putExtras(b);
  			             			Main.this.startActivity(mainIntent);
  			        			
  			                        

  			                    }   });
  			    			}
  			    		});}
 	 		    	catch(MalformedURLException e1){
 			    		}catch(IOException e2){
 			    		}
 	 		    }
 	 		    }).start(); 
    		 
    		 
    	 }
    	 
else if(array.size()==2){
    		 
    		 new Thread(new Runnable() {		    
	 	 		    public void run() {
	 	 		    	
	 	 		    	try{
 	 		    	
  			    		InputStream is= new	URL(array.get(0).imagen).openStream();
  			    		@SuppressWarnings("deprecation")
 						final
  						Drawable imagen = new BitmapDrawable(BitmapFactory.decodeStream(is));
  			    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
  			    		
  			    		is= new	URL(array.get(1).imagen).openStream();
  			    		@SuppressWarnings("deprecation")
 						final
  						Drawable imagen2 = new BitmapDrawable(BitmapFactory.decodeStream(is));
  			    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
  			    		bar1.post(new Runnable() {
  			    			public void run() {
  			    			bar1.setBackgroundDrawable(imagen);
  			    			loading.setText("");
  			    			bar1.setOnClickListener(new OnClickListener() {
  			                    
  			                    public void onClick(View v) {
  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
  			             	        Bundle b = new Bundle();
  			             	        BarList barlist1 = new BarList();
  			             	        Bar barr = array.get(0);
  			             	        barlist1.add(barr);
  			             	        b.putParcelable("Bar",barlist1);
  			             	        mainIntent.putExtras(b);
  			             			Main.this.startActivity(mainIntent);
  			        			
  			                        

  			                    }   });
  			    			}
  			    		});
  			    		bar2.post(new Runnable() {
  			    			public void run() {
  			    			bar2.setBackgroundDrawable(imagen2);
  			    			bar2.setOnClickListener(new OnClickListener() {
  			                    
  			                    public void onClick(View v) {
  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
  			             	        Bundle b = new Bundle();
  			             	        BarList barlist1 = new BarList();
  			             	        Bar barr = array.get(1);
  			             	        barlist1.add(barr);
  			             	        b.putParcelable("Bar",barlist1);
  			             	        mainIntent.putExtras(b);
  			             			Main.this.startActivity(mainIntent);
  			        			
  			                        

  			                    }   });
  			    			}
  			    		});
  			    		bar3.post(new Runnable() {
  			    			public void run() {
  			    			bar3.setBackgroundDrawable(imagen);
  			    			bar3.setOnClickListener(new OnClickListener() {
  			                    
  			                    public void onClick(View v) {
  			                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
  			             	        Bundle b = new Bundle();
  			             	        BarList barlist1 = new BarList();
  			             	        Bar barr = array.get(0);
  			             	        barlist1.add(barr);
  			             	        b.putParcelable("Bar",barlist1);
  			             	        mainIntent.putExtras(b);
  			             			Main.this.startActivity(mainIntent);
  			        			
  			                        

  			                    }   });
  			    			}
  			    		});}
 	 		    	catch(MalformedURLException e1){
 	 		    	}catch(IOException e2){
		    		}
	 	 		    }
 		    }).start(); 
    		 
    	 
    		 }
    	 
    	 else if(array.size()>2){
    		 Log.e("MAYOR QUE 3","MAYOR QUE 3"); 
    		 new Thread(new Runnable() {		    
	 	 		    public void run() {
	 	 		    	
	 	 		    	try{
    		 InputStream is= new	URL(array.get(0).imagen).openStream();
	    		@SuppressWarnings("deprecation")
				final
				Drawable imagen = new BitmapDrawable(BitmapFactory.decodeStream(is));
	    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
	    		
	    		is= new	URL(array.get(1).imagen).openStream();
	    		@SuppressWarnings("deprecation")
				final
				Drawable imagen2 = new BitmapDrawable(BitmapFactory.decodeStream(is));
	    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
	    		
	    		is= new	URL(array.get(2).imagen).openStream();
	    		@SuppressWarnings("deprecation")
				final
				Drawable imagen3 = new BitmapDrawable(BitmapFactory.decodeStream(is));
	    		imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
	    		bar1.post(new Runnable() {
	    			public void run() {
	    			bar1.setBackgroundDrawable(imagen);
	    			loading.setText("");
	    			bar1.setOnClickListener(new OnClickListener() {
		                    
		                    public void onClick(View v) {
		                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
		             	        Bundle b = new Bundle();
		             	        BarList barlist1 = new BarList();
		             	        Bar barr = array.get(0);
		             	        barlist1.add(barr);
		             	        b.putParcelable("Bar",barlist1);
		             	        mainIntent.putExtras(b);
		             			Main.this.startActivity(mainIntent);
		        			
		                        

		                    }   });
	    			}
	    		});
	    		bar2.post(new Runnable() {
	    			public void run() {
	    			bar2.setBackgroundDrawable(imagen2);
	    			bar2.setOnClickListener(new OnClickListener() {
		                    
		                    public void onClick(View v) {
		                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
		             	        Bundle b = new Bundle();
		             	        BarList barlist1 = new BarList();
		             	        Bar barr = array.get(1);
		             	        barlist1.add(barr);
		             	        b.putParcelable("Bar",barlist1);
		             	        mainIntent.putExtras(b);
		             			Main.this.startActivity(mainIntent);
		        			
		                        

		                    }   });
	    			}
	    		});
	    		bar3.post(new Runnable() {
	    			public void run() {
	    			bar3.setBackgroundDrawable(imagen3);
	    			bar3.setOnClickListener(new OnClickListener() {
		                    
		                    public void onClick(View v) {
		                    	 Intent mainIntent = new Intent(Main.this, BarDetail.class);
		             	        Bundle b = new Bundle();
		             	        BarList barlist1 = new BarList();
		             	        Bar barr = array.get(2);
		             	        barlist1.add(barr);
		             	        b.putParcelable("Bar",barlist1);
		             	        mainIntent.putExtras(b);
		             			Main.this.startActivity(mainIntent);
		        			
		                        

		                    }   });
	    			}
	    		}
	    		);
	    		canclick=true;
}
	    	catch(MalformedURLException e1){
	    		}catch(IOException e2){
	    		}
	    }
	    }).start(); 
    		 
    		 
    		 
    	 }
    	 
    	 
	       
    	 bar1.setClickable(true);
    	 bar2.setClickable(true);
    	 barizq.setEnabled(true);
    	     	 
    	 
    	 
    	 bar3.setClickable(true);
    	 barder.setClickable(true);
    	 barizq.setClickable(true);
    	 
    	 Log.e("adad","adasdasdassdsadsadgsajfbadvjeilsehviehlvikesnvlisehnvilesbviesbvlwes");
    	}


}
