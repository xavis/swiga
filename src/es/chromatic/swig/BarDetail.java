package es.chromatic.swig;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BarDetail extends Activity {

	TextView titulo;
	TextView subtitulo;
	TextView direccion;
	TextView telefono;
	Button fav;
	Button map;
	Button phone;
	BarList barlist;
	Bar bar;
	SQL dataHelper;
	Boolean inFav;
	DealList array;
	Button bar1;
	Button bar2;
	Button bar3;
	Button barder;
	Button barizq;
	TextView loading;
	Deal currentDeal;
	boolean canclick=false;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bar);

		titulo = (TextView) findViewById(R.id.bartitle);
		subtitulo =  (TextView) findViewById(R.id.barsubtitulo);
		direccion =  (TextView) findViewById(R.id.bardireccion);
		telefono =  (TextView) findViewById(R.id.bartelefono);
		fav =  (Button) findViewById(R.id.fav);
		phone =(Button) findViewById(R.id.phonebutton);
		map = (Button) findViewById(R.id.mapabutton);
		bar1 = (Button)findViewById(R.id.deal1);
		bar2 = (Button)findViewById(R.id.deal2);
		bar3 = (Button)findViewById(R.id.deal3);
		barder = (Button)findViewById(R.id.dealder);
		barizq = (Button)findViewById(R.id.dealizq);
		loading = (TextView) findViewById(R.id.loadtext2);

		Bundle b = getIntent().getExtras();

		barlist = b.getParcelable("Bar");
		bar = barlist.get(0);
		titulo.setText(bar.nombre);
		subtitulo.setText(bar.tipo);
		direccion.setText(bar.direccion);
		telefono.setText(bar.telefono);
		bar1.setBackgroundDrawable(null);
		bar2.setBackgroundDrawable(null);
   	 	bar3.setBackgroundDrawable(null);

		checkBar(bar.id);


		new Thread(new Runnable() {		    
			public void run() {
				final ImageView imageView =	(ImageView)findViewById(R.id.barimagen);
				try{
					final InputStream is= new	URL(bar.imagen).openStream();
					@SuppressWarnings("deprecation")
					
					final
					Drawable imagen = new BitmapDrawable(BitmapFactory.decodeStream(is));
					
					
					imagen.setBounds(0, 0, getWindow().getAttributes().width,getWindow().getAttributes().height);
					imageView.post(new Runnable() {
						public void run() {
							Bitmap bitmap = ((BitmapDrawable)imagen).getBitmap();
							ByteArrayOutputStream out = new ByteArrayOutputStream();
							
							
						if(bitmap!=null){
							imageView.setImageDrawable(imagen);
							}
						}
					});}
				catch(MalformedURLException e1){
				}catch(IOException e2){
				}
			}
		}).start();
		
		
		DownloadFilesTask1 download = new DownloadFilesTask1();
		download.execute();
		
	}


	public void call(View v) {
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:"+bar.telefono.replace(" ", "")));
			startActivity(callIntent);
		} catch (ActivityNotFoundException e) {
			Log.e("helloandroid dialing example", "Llamada fallida", e);
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Error");
			alertDialog.setMessage("La llamada ha fallado");
			alertDialog.setButton(1,"OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					// here you can add functions
				}
			});
			alertDialog.show();
		}
	}

	public void map(View v) {
		String uri = String.format("geo:%f,%f",Float.parseFloat(bar.latitud.toString()), Float.parseFloat(bar.longitud.toString()));
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		this.startActivity(intent);

	}

	public void fav(View v){
		if(this.dataHelper==null){
			this.dataHelper = new SQL(getApplicationContext());
		}
		if(!inFav){
			Log.e("FAVORITOS", "inFavfalso");
			dataHelper.insert(bar.id, bar.nombre, bar.tipo, bar.direccion, bar.localidad, bar.provincia, bar.longitud, bar.latitud, bar.telefono, bar.imagen);
			inFav=true;
		}

		else if(inFav){
			Log.e("FAVORITOS", "inFavcierto");
			this.dataHelper.delete(bar.id);
			inFav = false;

		}
		this.changeStar();
		Log.e("FAVORITOS", "ENTRA EN FAV");
	}


	public void changeStar(){
		this.checkBar(this.bar.id);
		if(inFav){
			Drawable imagen = getResources().getDrawable( R.drawable.star2 );
			this.fav.setBackgroundDrawable(imagen);
			Log.e("FAVORITOS", "inFavTRUE");
		}

		else if(!inFav){

			Drawable imagen = getResources().getDrawable( R.drawable.star1 );
			this.fav.setBackgroundDrawable(imagen);
			Log.e("FAVORITOS", "inFavFALSE");
		}



	}


	public void checkBar(String id){
		Log.e("FAVORITOS", "CHEKEA");
		if(this.dataHelper==null){
			this.dataHelper = new SQL(this);
		}
		inFav=false;

		BarList barlist = this.dataHelper.selectAll();


		Log.e("CHEQUEAS", "Tama�o de barlist: "+String.valueOf(barlist.size()));
		int i;
		for(i = 0; i<barlist.size();i++){
			Bar barer = barlist.get(i);
			Log.e("SELECTALL","ID: "+barer.id+" - "+bar.id);
			if(barer.id.equals(this.bar.id)){
				Log.e("FAVORITOS", "SE ENCUENTRA EL BAR");
				inFav=true;
				Drawable imagen = getResources().getDrawable( R.drawable.star2 );
				this.fav.setBackgroundDrawable(imagen);
				Log.e("FAVORITOS", "inFavTRUE");
			}
		}
	}

	public void onRightClick(View v){
		if(canclick==true){
			bar1.setBackgroundDrawable(null);
			bar2.setBackgroundDrawable(null);
			bar3.setBackgroundDrawable(null);
			loading.setText("Cargando...");
			if(array.size()>0&&array.size()>1){
				canclick=false;
				Log.e("OnRight","ENTRA");

				Deal last = array.get(array.size()-1);
				DealList sustituto = new DealList();
				sustituto.add(last);
				for(int i=0;i<array.size()-1;i++){

					sustituto.add(array.get(i));

				}

				array=sustituto;
				this.changeBars();}
		}

	}


	public void onLeftClick(View v){

		if(canclick==true){
			bar1.setBackgroundDrawable(null);
			bar2.setBackgroundDrawable(null);
			bar3.setBackgroundDrawable(null);
			loading.setText("Cargando...");
			if(array.size()>0&&array.size()>1){
				canclick=false;
				Log.e("OnLeft","ENTRA");

				Deal last = array.get(0);
				DealList sustituto = new DealList();
				for(int i=1;i<array.size();i++){

					sustituto.add(array.get(i));

				}
				sustituto.add(last);
				array=sustituto;
				this.changeBars();}}

	}







	private class DownloadFilesTask1 extends AsyncTask<Void, Integer, Void> {


		protected Void doInBackground(Void... voids) {
			if(isNetworkAvailable(1)){
			try {
				
				URL text = new URL("http://www.swagger.hostoi.com/bar_deal.php?key="+bar.id);

				array = new DealList();

				XmlPullParserFactory parserCreator =

						XmlPullParserFactory.newInstance();

				XmlPullParser parser = parserCreator.newPullParser();


				parser.setInput(text.openStream(), null);

				int parserEvent = parser.getEventType();

				while (parserEvent != XmlPullParser.END_DOCUMENT) {

					switch (parserEvent) {

					case XmlPullParser.START_DOCUMENT:						 
						break;

					case XmlPullParser.END_DOCUMENT:

						this.onPostExecute();

						break;

					case XmlPullParser.START_TAG:

						String tag = parser.getName();

						if (tag.equalsIgnoreCase("oferta")) {

							Log.i("XML","Creamos oferta");

							currentDeal = new Deal();							 


						}



						else if(tag.equalsIgnoreCase("nombre")){

							currentDeal.setNombre(parser.nextText());								 

						}

						else if(tag.equalsIgnoreCase("tipo")){

							currentDeal.setTipo(parser.nextText());								 

						}

						else if(tag.equalsIgnoreCase("hasta")){

							currentDeal.setHasta(parser.nextText());								 

						}

						else if(tag.equalsIgnoreCase("descripcion")){

							currentDeal.setDescripcion(parser.nextText());								 

						}

						else if(tag.equalsIgnoreCase("hora")){

							currentDeal.setHora(parser.nextText());								 

						}



						break;

					case XmlPullParser.END_TAG:	

						String tag2 = parser.getName();
						
						

						if (tag2.equalsIgnoreCase("oferta")) {
							currentDeal.barid=bar.id;
							currentDeal.barnombre=bar.nombre;
							currentDeal.bartipo = bar.tipo;
							currentDeal.bardireccion=bar.direccion;
							currentDeal.barlocalidad=bar.localidad;
							currentDeal.barprovincia=bar.provincia;
							currentDeal.bartelefono=bar.telefono;
							currentDeal.barimagen=bar.imagen;
							currentDeal.barlatitud=bar.latitud;
							currentDeal.barlongitud=bar.longitud;
							Log.i("XML","a�adimos oferta");

							array.add(currentDeal);

							currentDeal = null;


						}

						else if (tag2.equalsIgnoreCase("result")) {

							this.onPostExecute();


						}
						break;

					}

					parserEvent = parser.next();

				}

			} catch (Exception e) {				 
				Log.e("Net", "Error in network call", e);

			}}
			else 			loading.setText("No hay conexi�n");
				
				
			

			return null;
		}

		protected void onProgressUpdate(Integer... progress) {

		}

		protected void onPostExecute() {
			Log.i("Fin","ENTRA AL FINAL");
			
			if(array.size()==0){
				
				runOnUiThread(new Runnable() {
				     public void run() {
				    	 bar1.setBackgroundDrawable(null);
				    	 bar2.setBackgroundDrawable(null);
				    	 bar3.setBackgroundDrawable(null);
				    	 loading.setText("No hay ofertas");

				    }
				});

			}

			else if(array.size()==1){
				Log.d("ENTRA EN 1", "ENTRA EN 1");
				runOnUiThread(new Runnable() {
				     public void run() {

				    	 loading.setText("");

				    }
				});

				
						
						bar1.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								Log.d("CLICK", "CLICK");
								Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
								Bundle b = new Bundle();
								DealList barlist1 = new DealList();
								Deal barr = array.get(0);
								barr.setBar(barlist);
								barlist1.add(barr);
								b.putParcelable("Deal",barlist1);
								mainIntent.putExtras(b);
								BarDetail.this.startActivity(mainIntent);



							}   });



								bar2.setOnClickListener(new OnClickListener() {

									public void onClick(View v) {
										Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
										Bundle b = new Bundle();
										DealList barlist1 = new DealList();
										Deal barr = array.get(0);
										barr.bar = barlist;
										barlist1.add(barr);
										b.putParcelable("Deal",barlist1);
										mainIntent.putExtras(b);
										BarDetail.this.startActivity(mainIntent);



									}   });

							
			    			
								bar3.setOnClickListener(new OnClickListener() {

									public void onClick(View v) {
										Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
										Bundle b = new Bundle();
										DealList barlist1 = new DealList();
										Deal barr = array.get(0);
										barr.bar = barlist;
										barlist1.add(barr);
										b.putParcelable("Deal",barlist1);
										mainIntent.putExtras(b);
										BarDetail.this.startActivity(mainIntent);



							
					}});
			}




			else if(array.size()==2){

				new Thread(new Runnable() {		    
					public void run() {

						runOnUiThread(new Runnable() {
						     public void run() {

						    	 loading.setText("");

						    }
						});
						bar1.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
								Bundle b = new Bundle();
								DealList barlist1 = new DealList();
								Deal barr = array.get(0);
								barr.bar = barlist;
								barlist1.add(barr);
								b.putParcelable("Deal",barlist1);
								mainIntent.putExtras(b);
								BarDetail.this.startActivity(mainIntent);



							}   });



						bar2.post(new Runnable() {
							public void run() {

								bar2.setOnClickListener(new OnClickListener() {

									public void onClick(View v) {
										Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
										Bundle b = new Bundle();
										DealList barlist1 = new DealList();
										Deal barr = array.get(1);
										barr.bar = barlist;
										barlist1.add(barr);
										b.putParcelable("Deal",barlist1);
										mainIntent.putExtras(b);
										BarDetail.this.startActivity(mainIntent);



									}   });

							}});


						bar3.post(new Runnable() {
							public void run() {	
								
								runOnUiThread(new Runnable() {
								     public void run() {

								    	 loading.setText("");

								    }
								});
								bar3.setOnClickListener(new OnClickListener() {

									public void onClick(View v) {
										Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
										Bundle b = new Bundle();
										DealList barlist1 = new DealList();
										Deal barr = array.get(0);
										barr.bar = barlist;
										barlist1.add(barr);
										b.putParcelable("Deal",barlist1);
										mainIntent.putExtras(b);
										BarDetail.this.startActivity(mainIntent);



									}   });
							}});
					}});
			}

			else if(array.size()>=3){

				new Thread(new Runnable() {		    
					public void run() {

						runOnUiThread(new Runnable() {
						     public void run() {

						    	 loading.setText("");

						    }
						});
						bar1.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
								Bundle b = new Bundle();
								DealList barlist1 = new DealList();
								Deal barr = array.get(0);
								barr.bar = barlist;
								barlist1.add(barr);
								b.putParcelable("Deal",barlist1);
								mainIntent.putExtras(b);
								BarDetail.this.startActivity(mainIntent);



							}   });




								bar2.setOnClickListener(new OnClickListener() {

									public void onClick(View v) {
										Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
										Bundle b = new Bundle();
										DealList barlist1 = new DealList();
										Deal barr = array.get(1);
										barr.bar = barlist;
										barlist1.add(barr);
										b.putParcelable("Deal",barlist1);
										mainIntent.putExtras(b);
										BarDetail.this.startActivity(mainIntent);



									}   });

							


									    			
								bar3.setOnClickListener(new OnClickListener() {

									public void onClick(View v) {
										Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
										Bundle b = new Bundle();
										DealList barlist1 = new DealList();
										Deal barr = array.get(2);
										barr.bar = barlist;
										barlist1.add(barr);
										b.putParcelable("Deal",barlist1);
										mainIntent.putExtras(b);
										BarDetail.this.startActivity(mainIntent);



							
					}});
								
					}});
			}


			bar1.setClickable(true);
			bar2.setClickable(true);



			bar3.setClickable(true);
			barder.setClickable(true);
			barizq.setClickable(true);
			canclick=true;

			Log.e("adad","adasdasdassdsadsadgsajfbadvjeilsehviehlvikesnvlisehnvilesbviesbvlwes");
		}
	}


	public void changeBars(){
		if(array.size()==1){
			new Thread(new Runnable() {		    
				public void run() {

					runOnUiThread(new Runnable() {
					     public void run() {

					    	 loading.setText("");

					    }
					});
					bar1.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
							Bundle b = new Bundle();
							DealList barlist1 = new DealList();
							Deal barr = array.get(0);
							barr.bar = barlist;
							barlist1.add(barr);
							b.putParcelable("Deal",barlist1);
							mainIntent.putExtras(b);
							BarDetail.this.startActivity(mainIntent);



						}   });



					

							bar2.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
									Bundle b = new Bundle();
									DealList barlist1 = new DealList();
									Deal barr = array.get(0);
									barr.bar = barlist;
									barlist1.add(barr);
									b.putParcelable("Deal",barlist1);
									mainIntent.putExtras(b);
									BarDetail.this.startActivity(mainIntent);



								

						}});


					  			    			
							bar3.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
									Bundle b = new Bundle();
									DealList barlist1 = new DealList();
									Deal barr = array.get(0);
									barr.bar = barlist;
									barlist1.add(barr);
									b.putParcelable("Deal",barlist1);
									mainIntent.putExtras(b);
									BarDetail.this.startActivity(mainIntent);



								
						}});
				}});
		}




		else if(array.size()==2){

			new Thread(new Runnable() {		    
				public void run() {

					runOnUiThread(new Runnable() {
					     public void run() {

					    	 loading.setText("");

					    }
					});
					bar1.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
							Bundle b = new Bundle();
							DealList barlist1 = new DealList();
							Deal barr = array.get(0);
							barr.bar = barlist;
							barlist1.add(barr);
							b.putParcelable("Deal",barlist1);
							mainIntent.putExtras(b);
							BarDetail.this.startActivity(mainIntent);



						}   });




							bar2.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
									Bundle b = new Bundle();
									DealList barlist1 = new DealList();
									Deal barr = array.get(1);
									barr.bar = barlist;
									barlist1.add(barr);
									b.putParcelable("Deal",barlist1);
									mainIntent.putExtras(b);
									BarDetail.this.startActivity(mainIntent);



								

						}});


						    			
							bar3.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
									Bundle b = new Bundle();
									DealList barlist1 = new DealList();
									Deal barr = array.get(0);
									barr.bar = barlist;
									barlist1.add(barr);
									b.putParcelable("Deal",barlist1);
									mainIntent.putExtras(b);
									BarDetail.this.startActivity(mainIntent);



								
						}});
				}});
		}

		else if(array.size()>=3){

			new Thread(new Runnable() {		    
				public void run() {

					runOnUiThread(new Runnable() {
					     public void run() {

					    	 loading.setText("");

					    }
					});
					bar1.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
							Bundle b = new Bundle();
							DealList barlist1 = new DealList();
							Deal barr = array.get(0);
							barr.bar = barlist;
							barlist1.add(barr);
							b.putParcelable("Deal",barlist1);
							mainIntent.putExtras(b);
							BarDetail.this.startActivity(mainIntent);



						}   });



					

							bar2.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
									Bundle b = new Bundle();
									DealList barlist1 = new DealList();
									Deal barr = array.get(1);
									barr.bar = barlist;
									barlist1.add(barr);
									b.putParcelable("Deal",barlist1);
									mainIntent.putExtras(b);
									BarDetail.this.startActivity(mainIntent);




						}});


					 			    			
							bar3.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									Intent mainIntent = new Intent(BarDetail.this, DealDetail.class);
									Bundle b = new Bundle();
									DealList barlist1 = new DealList();
									Deal barr = array.get(2);
									barr.bar = barlist;
									barlist1.add(barr);
									b.putParcelable("Deal",barlist1);
									mainIntent.putExtras(b);
									BarDetail.this.startActivity(mainIntent);



								}   });
					
				}});
		}
		canclick=true;



		bar1.setClickable(true);
		bar2.setClickable(true);
		barizq.setEnabled(true);



		bar3.setClickable(true);
		barder.setClickable(true);
		barizq.setClickable(true);

		Log.e("adad","adasdasdassdsadsadgsajfbadvjeilsehviehlvikesnvlisehnvilesbviesbvlwes");


	}
	
public void backkk(View view){
		
		finish();
		
	}

public boolean isNetworkAvailable(int o) {
	 Context context = getApplicationContext();
	 ConnectivityManager connectivity = (ConnectivityManager) 
	    context.getSystemService(Context.CONNECTIVITY_SERVICE);
	 if (connectivity == null) {
		 if(o==0)Toast.makeText(getApplicationContext(), "No hay conexi�n", Toast.LENGTH_SHORT).show();
	 } else {
	  NetworkInfo[] info = connectivity.getAllNetworkInfo();
	  if (info != null) {
	   for (int i = 0; i < info.length; i++) {
	    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
	     return true;
	    }
	   }
	  }
	 }
	 if(o==0)Toast.makeText(getApplicationContext(), "No hay conexi�n", Toast.LENGTH_SHORT).show();
	 return false;
	}
	
public void home(View view){
		
	Intent mainIntent = new Intent(BarDetail.this, Main.class);
		BarDetail.this.startActivity(mainIntent);
		this.finish();
		
	}

}