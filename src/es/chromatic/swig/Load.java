package es.chromatic.swig;

import java.lang.reflect.Array;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class Load extends Activity{
	
	Bar currentBar;
	Deal currentDeal;
	BarList array;
	DealList array2;
	ProgressDialog mDialog;
	String key;
	int opcion;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_load);
	        
	        
	        Bundle extras = getIntent().getExtras();
	        key = extras.getString("String");
	        opcion = extras.getInt("Opcion");
	        if(key==null){
	        	key="";
	        }
	        if(opcion==0){
	        	mDialog = ProgressDialog.show(this, "", "Cargando bares", true);
	        	mDialog.setCancelable(true);
	        String string = "http://www.swagger.hostoi.com/bar_search.php?key="+key;
	        
	        DownloadFilesTask1 download = new DownloadFilesTask1();
	        download.execute(string);}
	        
	        else if(opcion==1){
	        	mDialog = ProgressDialog.show(this, "", "Cargando ofertas", true);
		        String string = "http://www.swagger.hostoi.com/deal_search.php?key="+key;
		        
		        DownloadFilesTask2 download = new DownloadFilesTask2();
		        download.execute(string);}
	       
	     
	    }
	 
	 private class DownloadFilesTask1 extends AsyncTask<String, Integer, Void> {
	    
		 
		 protected Void doInBackground(String... strings) {
			 try {
				 Log.i("DIRECCION",strings[0]);
				 URL text = new URL(strings[0]);
				 
				 array = new BarList();
				 
				 XmlPullParserFactory parserCreator =
							 
							 XmlPullParserFactory.newInstance();
					 
					 XmlPullParser parser = parserCreator.newPullParser();
					 
					 
					 parser.setInput(text.openStream(), null);
					 
					 int parserEvent = parser.getEventType();
					 
					 while (parserEvent != XmlPullParser.END_DOCUMENT) {
						 
						 switch (parserEvent) {
						 
						 case XmlPullParser.START_DOCUMENT:						 
							 break;
							 
						 case XmlPullParser.END_DOCUMENT:
							 
							 this.onPostExecute();
							 
							 break;
							 
						 case XmlPullParser.START_TAG:
							 
							 String tag = parser.getName();
							 
							 if (tag.equalsIgnoreCase("bar")) {
								 
								 Log.i("XML","Creamos bar");
								 
								 currentBar = new Bar();							 
										 
								 
							 }
							 
							 else if(tag.equalsIgnoreCase("id")){
								 
								 currentBar.setID(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("nombre")){
								 
								 currentBar.setNombre(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("tipo")){
								 
								 currentBar.setTipo(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("localidad")){
								 
								 currentBar.setLocalidad(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("provincia")){
								 
								 currentBar.setProvincia(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("CoordX")){
								 
								 currentBar.setLongitud(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("CoordY")){
								 
								 currentBar.setLatitud(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("direccion")){
								 
								 currentBar.setDireccion(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("telefono")){
								 
								 currentBar.setTelefono(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("imagen")){
								 
								 currentBar.setImagen(parser.nextText());								 
							 
							 }
							 
							 
							 
							 break;
							 
						 case XmlPullParser.END_TAG:	
							 
							 	String tag2 = parser.getName();
							 
							 if (tag2.equalsIgnoreCase("bar")) {
								 
								 Log.i("XML","a�adimos bar");
								 
								 array.add(currentBar);
								 
								 currentBar = null;
										 
								 
							 }
							 
							 else if (tag2.equalsIgnoreCase("result")) {
								 
								 this.onPostExecute();
										 
								 
							 }
							 break;
							 
						 }
						 
						 parserEvent = parser.next();
						 
					 }
					 
				 } catch (Exception e) {				 
					 Log.e("Net", "Error in network call", e);
					 
				 }
			 
		        return null;
		     }

	     protected void onProgressUpdate(Integer... progress) {
	        
	     }

	     protected void onPostExecute() {
	    	 Log.i("Fin","ENTRA AL FINAL");
		        mDialog.dismiss();
		        
		        Intent mainIntent = new Intent(Load.this, Table.class);
		        Bundle b = new Bundle();
		        b.putParcelable("Results", array);
		        b.putInt("Opcion", opcion);
		        mainIntent.putExtras(b);
				Load.this.startActivity(mainIntent);
				
				Load.this.finish();
		     }
	 }
	 
 private class DownloadFilesTask2 extends AsyncTask<String, Integer, Void> {
	    
		 
		 protected Void doInBackground(String... strings) {
			 try {
				 Log.i("DIRECCION",strings[0]);
				 URL text = new URL(strings[0]);
				 
				 array2 = new DealList();
				 
				 XmlPullParserFactory parserCreator =
							 
							 XmlPullParserFactory.newInstance();
					 
					 XmlPullParser parser = parserCreator.newPullParser();
					 
					 
					 parser.setInput(text.openStream(), null);
					 
					 int parserEvent = parser.getEventType();
					 
					 while (parserEvent != XmlPullParser.END_DOCUMENT) {
						 
						 switch (parserEvent) {
						 
						 case XmlPullParser.START_DOCUMENT:						 
							 break;
							 
						 case XmlPullParser.END_DOCUMENT:
							 
							 this.onPostExecute();
							 
							 break;
							 
						 case XmlPullParser.START_TAG:
							 
							 String tag = parser.getName();
							 
							 if (tag.equalsIgnoreCase("oferta")) {
								 
								 Log.i("XML","Creamos oferta");
								 
								 currentDeal = new Deal();							 
										 
								 
							 }
							 
							 
							 
							 else if(tag.equalsIgnoreCase("nombre")){
								 
								 currentDeal.setNombre(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("tipo")){
								 
								 currentDeal.setTipo(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("hasta")){
								 
								 currentDeal.setHasta(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("descripcion")){
								 
								 currentDeal.setDescripcion(parser.nextText());								 
							 
							 }
							 
							 else if(tag.equalsIgnoreCase("hora")){
								 
								 currentDeal.setHora(parser.nextText());								 
							 
							 }
							 
							 
							 
							 break;
							 
						 case XmlPullParser.END_TAG:	
							 
							 	String tag2 = parser.getName();
							 
							 if (tag2.equalsIgnoreCase("oferta")) {
								 
								 Log.i("XML","a�adimos oferta");
								 
								 array2.add(currentDeal);
								 
								 currentDeal = null;
										 
								 
							 }
							 
							 else if (tag2.equalsIgnoreCase("result")) {
								 
								 this.onPostExecute();
										 
								 
							 }
							 break;
							 
						 }
						 
						 parserEvent = parser.next();
						 
					 }
					 
				 } catch (Exception e) {				 
					 Log.e("Net", "Error in network call", e);
					 
				 }
			 
		        return null;
		     }

	     protected void onProgressUpdate(Integer... progress) {
	        
	     }

	     protected void onPostExecute() {
	    	 Log.i("Fin","ENTRA AL FINAL");
		        mDialog.dismiss();
		        Intent mainIntent = new Intent(Load.this, Table.class);
		        Bundle b = new Bundle();
		        b.putParcelable("Results", array2);
		        b.putInt("Opcion", opcion);
		        mainIntent.putExtras(b);
				Load.this.startActivity(mainIntent);
				
				Load.this.finish();
		     }
	 }


}
