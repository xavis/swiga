package es.chromatic.swig; 
 
 
import java.util.ArrayList;
 
 
 
import android.os.Parcel;
 
import android.os.Parcelable;
 
 
 
public class DealList extends ArrayList<Deal> implements Parcelable{
 
 
 
        private static final long serialVersionUID = 663585476779879096L;
 
 
 
        public DealList(){
 
               
 
        }
 
       
 
        public DealList(Parcel in){
 
                readFromParcel(in);
 
        }
 
       
 
        @SuppressWarnings({ "unchecked", "rawtypes" })
 
        public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
 
                public DealList createFromParcel(Parcel in) {
 
                        return new DealList(in);
 
                }
 
 
 
                public Object[] newArray(int arg0) {
 
                        return null;
 
                }
 
 
 
        };
 
       
 
        private void readFromParcel(Parcel in) {
 
                this.clear();
 
 
 
                //First we have to read the list size
 
                int size = in.readInt();
 
 
 
                //Reading remember that we wrote first the Name and later the Phone Number.
 
                //Order is fundamental
 
               
 
                for (int i = 0; i < size; i++) {
 
                        Deal c = new Deal();
 
                        c.setNombre(in.readString());
                        
                        c.setTipo(in.readString());
 
                        c.setHora(in.readString());
                        
                        c.setHasta(in.readString());
                        
                        c.setDescripcion(in.readString());
                        
                        c.setBarNombre(in.readString());
                        
                        c.setBarTipo(in.readString());
                        
                        c.setBarLocalidad(in.readString());
                        
                        c.setBarProvincia(in.readString());
                        
                        c.setBarDireccion(in.readString());                      
                        
                        c.setBarLatitud(in.readString());
                        
                        c.setBarLongitud(in.readString());
                        
                        c.setBarID(in.readString());
                        
                        c.setBarImagen(in.readString());                        
                        
                        c.setBarTelefono(in.readString());
                        
                        this.add(c);
 
                }
 
               
 
        }
 
 
 
        public int describeContents() {
 
                return 0;
 
        }
 
 
 
        public void writeToParcel(Parcel dest, int flags) {
 
                int size = this.size();
 
                //We have to write the list size, we need him recreating the list
 
                dest.writeInt(size);
 
                //We decided arbitrarily to write first the Name and later the Phone Number.
 
                for (int i = 0; i < size; i++) {
 
                        Deal c = this.get(i);
 
                        dest.writeString(c.getNombre());
 
                        dest.writeString(c.getTipo());
                        
                        dest.writeString(c.getHora());
                        
                        dest.writeString(c.getHasta());
                        
                        dest.writeString(c.getDescripcion());
                        
                        dest.writeString(c.getBarNombre());
                        
                        dest.writeString(c.getBarTipo());
                        
                        dest.writeString(c.getBarLocalidad());
                        
                        dest.writeString(c.getBarProvincia());
                        
                        dest.writeString(c.getBarDireccion());
                        
                        dest.writeString(c.getBarLatitud());
                        
                        dest.writeString(c.getBarLongitud());
                        
                        dest.writeString(c.getBarID());
                        
                        dest.writeString(c.getBarImagen());
                        
                        dest.writeString(c.getBarTelefono());
                        
 
                }
 
        }
 
       
 
 
 
}
 
 